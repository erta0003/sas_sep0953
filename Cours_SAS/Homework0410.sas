/****** EXERCICES DE SAS SQL SEP0953 ******/
/****** GROUPE 2 *******/ 
/****** ERTAS Elif - SEJDI Benjamin - BRUNET Alexandre - LACROIX Amelie - THIERUS Armand *********** /
/****** MASTER 2 SEP 2023 - 2024 *****/

/**************************************************************************************************************/
/* Pour le 04/10 */
/* 1. (Benjamin) Les années de naissances des employés */

data BirthYears;
    set employees;
    birth_year = year(input(birthdate, yymmdd10.));
    format birth_year 4.;
    keep birth_year
run;


/* 2. (Benjamin) Liste des noms de produit avec des stocks supérieurs à 2 */

data ProductsWithUnitsInStock;
    set products;
    if unitsinstock > 2;
    keep productname;
run;

/* 3. (Elif) Le numero de commande de la commande avec le plus long délai de livraison (shippeddate - ordedate) */ 
DATA orders2;
	SET orders;
	
	test1 = substr(ShippedDate, 1,10);
	test2 = substr(OrderDate, 1,10);

	/* Convertir la variable ShippedDate en date */
	ShippedDate_dt=MDY(INPUT(SCAN(test1, 2, "-"), 2.), INPUT(SCAN(test1, 
		3, "-"), 2.), INPUT(SCAN(test1, 1, "-"), 4.));

	/* Convertir la variable OrderDate en date */
	OrderDate_dt=MDY(INPUT(SCAN(test2, 2, "-"), 2.), INPUT(SCAN(test2, 3, 
		"-"), 2.), INPUT(SCAN(test2, 1, "-"), 4.));
RUN; 
DATA duration_orders;
	SET orders2; 
	/* Calculer la durée (difference) entre ShippedDate et OrderDate */
	Duration=ShippedDate_dt - OrderDate_dt;
	keep OrderId Duration ;
RUN;
PROC SORT DATA=duration_orders NODUPKEY;
	By descending Duration;
RUN;
/* Le n° de commande 10660 est celle avec le plus long délai de livraison */

/* 4. (Amelie) Le nombre de fournisseur et le nombre d'entreprises de fournisseurs */

proc sort data=work.suppliers (keep= SupplierID CompanyName) nodupkey; 
	by SupplierID;
run;

/*Il y a 29 fournisseurs et comme les fournisseurs viennent tous d'entreprises différentes,
il y a 29 entreprises de fournisseurs*/

/* 5. (Amelie) Le nombre de clients avec la lettre F dans le prénom */

data clients_f (drop = ContactName);
	set work.customers (keep = ContactName);
	Prenom = scan(ContactName,1," ");
	Nom = scan(ContactName,2," ");
run;

data clients_f;
	set clients_f;
	where index(Prenom,"F");
run;

/*Il y a 4 clients avec un prénom contenant la lettre F*/

/* 6. (Alexandre) Comment fonctionne la variable ReportTo de la table employee */
/* Comment fonctionne la variable ReportTo de la table employee 
/ La variable ReportsTo fonctionne avec les variables "Country" et "TitleOfCourtesy". /
/ ReportsTo renvoie une valeur selon le pays (2 si c'est USA et 5 si c'est UK). Mais si le titre /
/ de courtoisie n'est pas valide (la valeur doit être soit, Mr, soit Mrs, soit Ms), alors ReportsTo considère /
/ qu'il n'y a personne auquel on peut se référer. */



/* 7. (Alexandre) Le nombre de fournisseurs par ville (proc summary ou proc freq ou proc means) */
proc freq data=work.suppliers;
table City;
run;
/* Les fournisseurs proviennent tous de villes différentes, donc 
il existe autant de provenance que de fournisseurs, donc 29 villes différentes. */

/* 8. (Elif) Le nombre de produits par nom de catégories (data merge) */
/* Tri des ensembles de données par CategoryID */
proc sort data=Categories;
  by CategoryID;
run;

proc sort data=Products;
  by CategoryID;
run;
/* Fusion des ensembles de données en utilisant la variable "CategoryID" */
data Merged_Cat_Pro;
  merge CATEGORIES PRODUCTS;
  by CategoryID;
run;

/* Création d'une nouvelle variable Count pour compter le nombre de produits par catégorie */
data ProductCountByCat;
  set Merged_Cat_Pro;
  by CategoryID;
  if first.CategoryID then Count = 1;
  else Count + 1;
  if last.CategoryID then output;
  drop CategoryID;
run;

/* Affichage des résultats */
proc print data=ProductCountByCat;
  title 'Nombre de produits par catégorie';
run;

/* 9. (Armand) Le nombre de commande par région où vivent les employés (data merge) */
proc sort data=employees;
    by region;
run;

proc sort data=orders;
    by shipregion;
run;


data MaTableUnique;
  set employees;
  by region;
  if first.region;
  keep region;
run;

data ShipRegionList;
  set orders;
  keep shipregion;
run;
data ShipRegionList_cleaned;
    merge ShipRegionList (in=a rename=(ShipRegion=region)) MaTableUnique (in=b);
    by region;
    if a=1 and b=1;
    drop a b;
run;
proc freq data=ShipRegionList_cleaned;
  tables region / out=EffectifsShipRegionList;
run;


/* 10. (Carlos) Le nombre de produits commandés par les numéros d'identifications des catégories */


data Orders1(drop=OrderID);
set Orders;
OrderI=input(OrderID,5.);
run;

Proc sort data= Orders1 ;
by OrderI;
run;


Proc sort data= Order_Details;
by OrderID;
run;

data Customer_Quantite;
merge Orders1 (in=a keep= CustomerID OrderI rename=(OrderI=OrderID))
Order_Details (in=b keep= Quantity ProductID OrderID ) ;
by OrderID;
if a;
run;

Proc sort data= Customer_Quantite;
by ProductID;
run;

Proc sort data= Products;
by ProductID;
run;

data Customer_Quantite_Categorie;
merge Customer_Quantite (in=a keep= CustomerID Quantity ProductID )
Products (in=b keep= ProductID CategoryID);
by ProductID;
if a;
run;

Proc sort data= Customer_Quantite_Categorie;
by CustomerID CategoryID;
run;

Data Res10 (keep= CustomerID CategoryID TotalQuant);
set Customer_Quantite_Categorie;
by CustomerID CategoryID;
if first.CategoryID=1 then TotalQuant=0;
TotalQuant+ Quantity;
if last.CategoryID=1;
run;


/**************************************************************************************************************/
 
