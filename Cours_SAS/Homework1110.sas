/****** EXERCICES DE SAS SQL SEP0953 ******/
/****** GROUPE 2 *******/ 
/****** ERTAS Elif - SEJDI Benjamin - BRUNET Alexandre - LACROIX Amelie - THIERUS Armand *********** /
/****** MASTER 2 SEP 2023 - 2024 *****/

/**************************************************************************************************************/
/* Pour le 11/10 */

/* 1. (Amélie) Combien d'employés et de quels sont l'âge de l'employé le plus jeune et du plus vieux [SELECT FONCTION] */

proc sql; 
	select * 
	from employees (keep = EmployeeID BirthDate)
	order by BirthDate desc; 
quit; 

/*Il y a 9 employés, le plus jeune a 57 ans (1966) et le plus vieux a 86 ans (1937) */

/* 2. (Amélie) Combien de commandes au total ? [SELECT AGGREGATION] */ 

proc sql;
	select count(*)
	from orders;
quit;

/*Il y a 830 commandes au total*/

/* 3. (Amélie) Quels sont les id des 5 fournisseurs à contacter car il n'y a plus de stocks [WHERE] */

proc sql;
	select SupplierID
	from suppliers
	where ContactTitle = "Marketing Manager";
quit;

/*Les id des fournisseurs à contacter sont : 4, 7, 10, 15 & 25*/

/* 4. (Elif) Quel est le CA total,le nombre de quantités, est le prix moyen par product ID, à trier du plus haut CA au plus bas ? [GROUP BY & ORDER BY] */

proc sql;
	select ProductID, SUM(Quantity*UnitPrice) as CA_total, 
	COUNT(Quantity) as nombre_qte,
	AVG(UnitPrice) as prix_moyen
	from Order_Details group by ProductID 
	order by CA_total desc;
quit;

/* 5. (Elif) Quelle est la liste des produts ID qui a généré plus de 10 000 de CA ? [HAVING] */

proc sql;
	create table Liste_productsID_more_10000 as 
    select ProductID
    from (
        select ProductID, SUM(Quantity*UnitPrice) as CA_total, 
            COUNT(Quantity) as nombre_qte,
            AVG(UnitPrice) as prix_moyen
        from Order_Details
        group by ProductID 
        having CA_total > 10000
    );
quit;

/* 6. (Elif) Quelle est la liste des noms de produits qui ont généré plus de 10 de CA ? [JOIN AVEC 2 TABLES] */

proc sql;
    select P.ProductName, O.ProductID
    from Liste_productsID_more_10000 as O
    inner join Products as P on O.ProductID = P.ProductID;
quit;

/* 7. Qui est le territoryID qui a généré plus de CA ? [JOIN AVEC 3 TABLES] */

proc sql;
    create table CA as select a.TerritoryID, b.EmployeeID, c.OrderID,d.Quantity,d.UnitPrice
    from EMPLOYEETERRITORIES as a
    inner join Employees as b on a.EmployeeID = b.EmployeeID
    inner join Orders as c on b.EmployeeID = c.EmployeeID;
    inner join Order_Details as d on c.OrderID = d.OrderID;
quit;

/* 8. Quel nom de catégorie de produit ont été le plus vendus par année ? [JOIN AVEC 4 TABLES] */

proc sql;
	create table year_sells as select a.CategoryName, d.OrderDate from Categories 
		as a inner join Products as b on a.CategoryID=b.CategoryID inner join 
		Order_Details as c on b.ProductID=c.ProductID inner join Orders as d on 
		c.OrderID=d.OrderID;
quit;

data year_sells;
	set year_sells;
	Annee=substr(OrderDate, 1, 4);
run;

proc sort data=work.year_sells;
	by descending UnitsOnOnder Annee;
run;

/* 9. (Elif) Combien d'employés sont des managers [AUTO-JOINTURE] */

proc sql;
    select count(distinct A.EmployeeID) as Nombre_Managers
    from EMPLOYEES as A, EMPLOYEES as B
    where A.ReportsTo = B.EmployeeID
      and B.Title like '%Manager%';
quit;

/* 10. Trier tous les noms de produits selon leurs popularité de ventes et en affichant les noms des catégories et ne garder que les
/* 2 produits les plus populaires parmis les produits qui ont vendus plus de 10 exemplaires et parmis les unitprice > 1 [COMPLET] */

Proc sql;
    create table Work.P_CA_C as 
    select  Distinct(b.ProductName), a.CategoryName,sum(Quantity*c.UnitPrice) as CA 
    from  WORK.CATEGORIES as a
    left join  WORK.PRODUCTS as b on a.categoryid=b.categoryid
    left join  WORK.ORDER_DETAILS as c on b.productid=c.productid
    where c.Unitprice > 1 
    group by 1
    order by  CA desc;
quit;



/**************************************************************************************************************/
 
