/****** EXERCICES DE SAS SQL SEP0953 ******/
/****** GROUPE 2 *******/ 
/****** ERTAS Elif - SEJDI Benjamin - BRUNET Alexandre - LACROIX Amelie - THIERUS Armand *********** /
/****** MASTER 2 SEP 2023 - 2024 *****/

/**************************************************************************************************************/
/* Pour le 17/10 */

/* 1. (Elif) La liste des ID de produits avec pour chacun, la quantité vendue et le CA généré (en prenant en compte les discount) */

proc sql;
	select ProductID
		,sum(Quantity*UnitPrice*(1-Discount)) as CA_total
		,sum(Quantity) as nombre_qt
	from Order_Details group by ProductID;
quit;



/* 2. (Elif) La liste des ID de clients avec pour chaque client, le nombre de commandes faites en 1997, le nombre de commandes faites en 1998 et le taux d'évolution entre les deux */ 

proc sql;
   create table Orders_1997_1998 as 
   select
      CustomerID,
      count(case when year(input(OrderDate, yymmdd10.)) = 1997 then 1 else . end) as Commandes_1997,
      count(case when year(input(OrderDate, yymmdd10.)) = 1998 then 1 else . end) as Commandes_1998
   from Orders
   group by CustomerID;
quit;

proc sql;
select 
      CustomerID,
      Commandes_1997, 
      Commandes_1998, 
      ((Commandes_1998 - Commandes_1997 )/Commandes_1997)*100 as Taux_Evolution
   from Orders_1997_1998;
quit;
	


/* 3.	(Armand) La liste des noms des produits pour lesquels il n'y a pas assez de stocks pour couvrir les commandes et combien et combien est ce qu'il faudra dépenser pour couvrir ces manques de stocks */
data nouvelle_table;
    set Products;
    if UnitsInStock < UnitsOnOrder;
    NouvelleColonne = (UnitsOnOrder-UnitsInStock) * UnitPrice;
    keep ProductName NouvelleColonne;
run;



/* 4.	(Armand)Le nom de l'entreprise, du contact et l'ID des fournisseurs qu'il faut contacter pour couvrir les stocks par ordre de priorité (en haut, les fournisseurs chez qui nous devront acheter le plus d'unités) */
data nouvelle_table;
    set Products;
    if UnitsInStock < UnitsOnOrder;
    NouvelleColonne = (UnitsOnOrder - UnitsInStock);
    keep ProductName SupplierID NouvelleColonne;
run;
proc sort data=nouvelle_table out=nouvelle_table_triee;
    by descending NouvelleColonne;
run;
proc sql;
    create table nouvelle_table_jointe as
    select A.SupplierID, B.CompanyName, B.ContactName
    from nouvelle_table_triee as A
    left join suppliers as B
    on A.SupplierID = B.SupplierID;
quit;



/* 5.	Le nom, prénoms et ID des employés qui ont fait le plus de commandes pour chaque année de commande */

data orders1; set Orders;
Annee=substr(OrderDate, 1, 4);
run;

proc sql;
create table best_orders as select A.EmployeeID, A.Lastname,A.FirstName, B.Annee, max(C.Quantity) 
from Employees as A left join orders1 as B on A.EmployeeID = B.EmployeeID
left join ORDER_DETAILS as C on B.OrderID = C.OrderID
Order by Annee desc, C.Quantity desc;
quit;
run;

/* 6.	Le nom, prénoms et ID des employés qui ont généré le plus discount (en valeur monétaires c'est à dire en prenant le CA de la commande * discount) pour chaque année de commande */

data orders2; set Orders;
Annee=substr(OrderDate, 1, 4);
run;

proc sql;
create table best_empl as select B.Annee,A.EmployeeID, A.Lastname,A.FirstName, (C.Discount*C.UnitPrice) as DiscountEmp
from Employees as A left join orders2 as B on A.EmployeeID = B.EmployeeID
left join ORDER_DETAILS as C on B.OrderID = C.OrderID
Order by DiscountEmp desc;
quit;
run;

proc sql;
create table best_empl as select B.Annee,A.EmployeeID, A.Lastname,A.FirstName, (C.Discount*C.UnitPrice) as DiscountEmp
from Employees as A left join orders2 as B on A.EmployeeID = B.EmployeeID
left join ORDER_DETAILS as C on B.OrderID = C.OrderID
Order by DiscountEmp desc;
quit;
run;

proc print data=best_empl (obs=1);
run;

/* 7.	Le nom, prénoms et ID des meilleurs employés (qui ont généré le plus de CA sans prise en compte du discount) pour chaque année de commande */

proc sql;
create table best_empl as select A.EmployeeID, A.Lastname,A.FirstName, (C.Quantity*C.UnitPrice) as CA
from Employees as A left join Orders as B on A.EmployeeID = B.EmployeeID
left join ORDER_DETAILS as C on B.OrderID = C.OrderID
Order by CA desc;
quit;
run;

/* 8.	Les descriptions de region qui ont générés le plus de CA sur l'ensemble de la période */

data ORDERS;
    set ORDERS;
    OrderID = strip(OrderID);
RUN;

data ORDERS1;
    set ORDERS;
    OrderID = input(OrderID, 4.);
RUN;


proc sql;
create table descri as select A.RegionDescription, (E.Quantity*E.UnitPrice*(1-E.Discount)) as CA
from Regions as A left join Territories as B on A.RegionID = B.RegionID
left join WORK.EMPLOYEETERRITORIES as C on B.TerritoryID = C.TerritoryID
left join WORK.Orders as D on C.EmployeeID = D.EmployeeID
left join WORK.ORDER_DETAILS as E on D.OrderID = E.OrderID;
quit;
run;


/* 9.	(Amélie) La liste des noms des catégories de produits en les triant par les moyennes des prix commandés en 1998 et en affichant les stocks disponibles de chaque produit */

proc sql;
	select  a.categoryname as nom_categorie
			,sum(c.unitprice*c.quantity*(1-c.Discount))/sum(c.quantity) as moyenne_prix
			,sum(b.unitsinstock) as stock_disponible
			
	from categories as a
		 left join products as b on a.CategoryID = b.CategoryID
		 left join order_details as c on b.ProductID = c.ProductID
		 left join orders as d on c.OrderID = input(d.OrderID,30.)
		 
	where substr(d.orderdate,1,4) = "1998"
	group by nom_categorie
	order by moyenne_prix;
quit;
 
/* 10.	Pour 1998, le CA (avec ou sans discount) généré pour la categorie ID 1 par chaque manager (en incluant les CA générés par les employés de chaque manger) */ 
 
